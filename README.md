# Make a wireframe and develop it

## OBJECTIVES
1. Create a web-page from a model in 2 days max
2. Create a wireframe for the pre-made model
3. Use the wireframe and model to create the web-page
4. Only use CSS and HTML
5. Use the correct HTML tags
6. implement Font Awesome

## TODO 
1. Create HTML Structure 
---- implemented rough frame
---- started work on nav
---- finished nav
---- started header
---- finished header
---- finished main
---- finished footer

2. Create CSS Structure 
---- created scss file
---- initialized a package.json
---- started work on nav
---- finished nav
---- started header
---- finished header
---- finished main

3. Add font Awesome library 
---- implemented font awesome

4. Nav 
---- implemented rough HTML structure
---- started scss
---- finished nav bar
---- added responsiveness
---- started burger menu
---- implemented rough burger menu
---- finished burger menu implementation

5. Header
---- implemented rough HTML structure
---- started scss
---- finished header
---- added responsiveness

6. Main
---- implemented rough HTML structure
---- started css
---- finished main
---- added responsiveness

7. Footer
---- implemented rough HTML structure
---- started css
---- finished footer
---- added responsiveness

## HOW IT WORKS
To display page, open index.html
You may also go to the following link --> https://erorua16.gitlab.io/lander-eval

## FURTHER DEVELOPMENT
1. In order to instal necessary packages use: npm i 
2. Compile scss to css with: npm run comp
